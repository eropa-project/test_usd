<?php

class EropaCsv
{

    /**
     * URL где курсы
     * @var string
     */
    protected $urlCurs="https://api.exchangeratesapi.io/";


    /**
     * Читаем файл , если там нету поле USD то создаем новый файл и переписываем результат
     * @param $name
     * @return int
     */
    public function readFile($name)
    {
        /**
         * Читаем файл
         */
        $file = fopen("datecsv/".$name,"r");
        $line=0;
        $curs=$this->getCursToday('USD');
        $data = array();
        while(!feof($file))
        {
            $data[] = fgetcsv($file);
            if(count($data[$line])>3)
                return 1;

            if($line==0){
                $data[$line]=array('0'=>'ORDER_ID','1'=>'TIMESTAMP','2'=>'RUB','3'=>'USD');
            }else{
                $elem= $data[$line];
                $udsRever=(float)$elem[2]*$curs;
                $data[$line]=array('0'=>$elem[0],'1'=>$elem[1],'2'=>$elem[2],'3'=>$udsRever);
            }

            $line++;
        }
        fclose($file);

        /**
         * Переписываем файл новыми значениями
         */
        $file = fopen("datecsv/".$name,"wb");
        foreach ($data as $line) {
            fputcsv($file, $line);
        }
        fclose($file);
    }

    /**
     *
     * @param $path Название папке где лежат файлы
     */
    public function getAllFile($path){
        echo "start \n";
        $path = $path."/";
        var_dump($path);
        $files = scandir($path);
        foreach ($files as $file){
            if($file!="." && $file!=".."){
                echo "To do - ".$file." \n";
                $this->readFile($file);
            }
        }
        echo "stop \n";
    }

    /**
     * Возрошаем курс на сегодня
     * @param $name Валюта за основу RUB
     * @return int Значние ваюты
     */
    protected function getCursToday($name){
        $dataNow=date('Y-m-d');
        $url=$this->urlCurs.$dataNow."?base=RUB&symbo";
        $json = file_get_contents($url);
        $obj = json_decode($json);
        $rates=$obj->rates;
        return (is_null($rates->$name)?0:$rates->$name);
    }
}